package planner.map.com.map3d.ThreeD;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Form {
    public List<Form> child = new ArrayList<>();
    protected List<Edge> edgesList=new ArrayList<>();

    public void addForm(Form form) {
        child.add(form);
    }

    public List<Edge> getEdgesList() {
        return edgesList;
    }

    public void clearEdges() {
        edgesList=new ArrayList<>();
    }

    public void addEdge (Location3D point1, Location3D point2) {
        edgesList.add(new Edge(point1,point2));
    }

    public void addClosedPathEdges(List<Location3D> pointList) {
        Iterator iterator = pointList.iterator();
        Location3D first = (Location3D) iterator.next();
        Location3D p1 = first;
        Location3D p2 = first;
        while (iterator.hasNext()) {
            p2=(Location3D) iterator.next();
            edgesList.add(new Edge(p1,p2));
            p1=p2;
        }
        edgesList.add(new Edge(p1,first));
    }

    public void addOpenPathEdges(List<Location3D> pointList) {
        Iterator iterator = pointList.iterator();
        Location3D first = (Location3D) iterator.next();
        Location3D p1 = first;
        Location3D p2 = first;
        while (iterator.hasNext()) {
            p2=(Location3D) iterator.next();
            edgesList.add(new Edge(p1,p2));
            p1=p2;
        }
    }
}
