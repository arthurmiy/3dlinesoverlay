package planner.map.com.map3d;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import planner.map.com.map3d.MapInfosGetter.MapInfosGoogleMaps;
import planner.map.com.map3d.MapInfosGetter.MapInfosInterface;
import planner.map.com.map3d.ThreeD.Block;
import planner.map.com.map3d.ThreeD.Edge;
import planner.map.com.map3d.ThreeD.Location3D;
import planner.map.com.map3d.ThreeD.object3D;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    //GoogleMaps:
    private GoogleMap mMap;
    private final LatLng START_POSITION = new LatLng(-23.551522, -46.722234);

    //3D Overlay
    protected Canvas canvas;
    protected Paint paint = new Paint();
    protected ImageView im;

    //3D Obj data
    private List<Location3D> baseCoordnates=new ArrayList<>();
    private float  buildingHeight=40;
    private Block building;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Google map Setup
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //3d Overlay
        im = findViewById(R.id.img);

        baseCoordnates.add(new Location3D(new LatLng(-23.551291, -46.722145),0));
        baseCoordnates.add(new Location3D(new LatLng(-23.551379, -46.722494),0));
        baseCoordnates.add(new Location3D(new LatLng(-23.551694, -46.722376),0));
        baseCoordnates.add(new Location3D(new LatLng(-23.551604, -46.722049),0));
        building = new Block(baseCoordnates,buildingHeight);
        setupEnv();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //optional
        mMap.addMarker(new MarkerOptions().position(START_POSITION).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        //Move to start point
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(START_POSITION, mMap.getMaxZoomLevel() - 3));
        setMapCameraMoveListeners();
    }

    protected Point get3dPt(LatLng p, float height) {
        double tilt = (double) Math.toRadians(mMap.getCameraPosition().tilt);
        if (height!=0 && tilt!=0) {

            Point centerLin = new Point(im.getWidth() / 2, im.getHeight() / 2);//mMap.getProjection().toScreenLocation(mMap.getCameraPosition().target);

            Point pt = new Point();
            pt.set(centerLin.x, centerLin.y - im.getHeight() / 2);
            LatLng middleTop = mMap.getProjection().fromScreenLocation(pt);
            pt.set(centerLin.x, centerLin.y + im.getHeight() / 2);
            LatLng middleBottom = mMap.getProjection().fromScreenLocation(pt);
            pt.set(centerLin.x - im.getWidth() / 2, centerLin.y);
            LatLng middleLeft = mMap.getProjection().fromScreenLocation(pt);
            LatLng center = mMap.getProjection().fromScreenLocation(centerLin);


            //find camera height
            double tmp = Math.abs(SphericalUtil.computeDistanceBetween(middleTop, center) / SphericalUtil.computeDistanceBetween(middleBottom, center));
            tmp = ((1 - tmp) / (1 + tmp)) / Math.tan(tilt);
            tmp = Math.atan(tmp);
            tmp = Math.abs(SphericalUtil.computeDistanceBetween(middleBottom, center) * (Math.cos(tilt + tmp) / Math.sin(tmp)));

            double[] Vp = new double[2], VpX = new double[2], VpY = new double[2];
            double heading = SphericalUtil.computeHeading(center, middleTop);
            double pointHeading = SphericalUtil.computeHeading(center, p);
            double tmp2 = SphericalUtil.computeDistanceBetween(p, center);
            double multiplier = 1, multiplierx = 1;

            VpX[0] = tmp2 * Math.sin(Math.toRadians(pointHeading - heading));//modulo da componente x
            VpY[0] = tmp2 * Math.cos(Math.toRadians(pointHeading - heading));//modulo da componente y
            if (VpY[0] < 0) {
                VpY[0] = -VpY[0];
                VpY[1] = correctAngle(heading - 180);
                multiplier = -1;
            } else {
                VpY[1] = heading;
            }

            if (VpX[0] < 0) {
                VpX[0] = -VpX[0];
                VpX[1] = correctAngle(VpY[1] + 90 + 180);
                multiplierx = -1;

            } else {
                VpX[1] = correctAngle(VpY[1] + 90);
            }
            VpY[0] = VpY[0] + multiplier * height * Math.tan(tilt);
            VpY[1] = (VpY[0] < 0) ? correctAngle(VpY[1] - 180) : VpY[1];

            tmp2 = (double) 1 / (1 - ((height) / (Math.cos(tilt) * tmp)));

            Vp[0] = Math.sqrt(VpX[0] * VpX[0] + VpY[0] * VpY[0]) * tmp2;
            Vp[1] = (VpY[1] + Math.toDegrees(Math.atan(VpX[0] / VpY[0])) * multiplier * multiplierx);
            LatLng point = SphericalUtil.computeOffset(center, Vp[0], Vp[1]);
            return mMap.getProjection().toScreenLocation(point);
        } else {
            return mMap.getProjection().toScreenLocation(p);
        }
    }

    protected void draw3dLine(LatLng pFrom, float heightFrom, LatLng pTo, float heightTo) {
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        if (bounds.contains(pFrom) || bounds.contains(pTo)) {
            //draw only if one of the points are being displayed
            Point p1, p2;
            p1 = (get3dPt(pFrom, heightFrom));
            p2 = (get3dPt(pTo, heightTo));
            canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
        }
    }

    protected void drawEdge (Edge edge) {
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        if (bounds.contains(edge.getVertice1().getCoordinate()) || bounds.contains(edge.getVertice2().getCoordinate())) {
            //draw only if one of the points are being displayed
            Point p1, p2;
            p1 = (get3dPt(edge.getVertice1().getCoordinate(), edge.getVertice1().getRelativeAltitude()));
            p2 = (get3dPt(edge.getVertice2().getCoordinate(), edge.getVertice2().getRelativeAltitude()));
            canvas.drawLine(p1.x, p1.y, p2.x, p2.y, paint);
        }
    }

    protected double correctAngle(double Angle) {

        double result;
        if (Math.abs(Angle) > 180) {
            result = Angle % 360;
            if (result == 0) return result;
            result = result - 360 * result / Math.abs(result);

        } else {
            result = Angle;
        }

        return result;
    }


    private void clear3DOverlay(ImageView im) {
        Bitmap bm = getBlankBitmap(im.getWidth(), im.getHeight());
        canvas = new Canvas(bm);
        canvas.setBitmap(bm);
        im.setImageBitmap(bm);
    }

    private Bitmap getBlankBitmap(int width, int height) {
        return Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    }

    private void setMapCameraMoveListeners() {
        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                clear3DOverlay(im);
            }
        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                refresh3DView();
            }
        });
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                refresh3DView();
            }

        });
    }

    private void refresh3DView() {
        Bitmap bm = getBlankBitmap(im.getWidth(), im.getHeight());
        canvas = new Canvas(bm);
        if (canvas != null) {

            render (building);

        }

        im.setImageBitmap(bm);

    }

    public void refreshModel() {
        building = new Block(baseCoordnates,buildingHeight);
    }

    private void render(object3D Obj) {
        Iterator iterator = Obj.getEdgesList().iterator();
        while (iterator.hasNext()) {
            Edge tmp = (Edge) iterator.next();
            drawEdge(tmp);
        }
    }

    private void setupEnv () {
        SeekBar heightInput = findViewById(R.id.heightlevel);
        SeekBar thickness = findViewById(R.id.thickness);

        heightInput.setProgress((int) buildingHeight);
        setThickness(1);
        thickness.setProgress(1);

        heightInput.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ((AppCompatTextView) findViewById(R.id.height_lbl)).setText(getText(R.string.height)+" (" + progress + ")");
                buildingHeight=progress;
                refreshModel();
                refresh3DView();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                cameraDefaultPosition();
            }
        });

        thickness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setThickness(progress);
                refresh3DView();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                cameraDefaultPosition();

            }
        });
    }

    private void setThickness (float value) {
        paint.setStrokeWidth(value);
    }

    private Point LatLon2pt (LatLng in) {
        return mMap.getProjection().toScreenLocation(in);
    }

    private void cameraDefaultPosition () {
        CameraPosition pos = new CameraPosition.Builder().
                target(START_POSITION).
                zoom(mMap.getMaxZoomLevel() - 3).
                bearing(45)
                .tilt(45)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(pos),null);
    }
}
