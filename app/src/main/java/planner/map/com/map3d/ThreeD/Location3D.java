package planner.map.com.map3d.ThreeD;

import com.google.android.gms.maps.model.LatLng;

public class Location3D {
    protected LatLng coordinate=null;
    protected float relativeAltitude=0;

    public Location3D (LatLng coordinate,float relativeAltitude) {
        this.coordinate=coordinate;
        this.relativeAltitude=relativeAltitude;
    }

    public float getRelativeAltitude() {
        return relativeAltitude;
    }

    public LatLng getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(LatLng coordinate) {
        this.coordinate = coordinate;
    }

    public void setRelativeAltitude(float relativeAltitude) {
        this.relativeAltitude = relativeAltitude;
    }
}
