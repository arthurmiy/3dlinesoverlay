package planner.map.com.map3d.ThreeD;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Block extends object3D {
    private float height, width, depth;


    public Block (float height, float width, float depth, Location3D position) {
        this.height=height;
        this.depth = depth;
        this.width = width;
        this.position = position;
        addClosedPathEdges(calcBase(position.relativeAltitude));
        addClosedPathEdges(calcBase(position.relativeAltitude+height));
        extrudePath(calcBase(position.relativeAltitude),height);
    }

    public Block (List<Location3D> baseVertices, float height) {
        this.height=height;
        addClosedPathEdges(baseVertices);
        addClosedPathEdges(moveInZAxis(baseVertices,height));
        extrudePath(baseVertices,height);
    }

    public Block (List<Location3D> baseVertices, float height, float altitude) {
        this.height=height;
        addClosedPathEdges(baseVertices);
        addClosedPathEdges(moveInZAxis(baseVertices,height));
        extrudePath(baseVertices,height);
    }

    private List<Location3D> calcBase(float altitude) {
        List<Location3D> baseVert = new ArrayList<>();
        LatLng tmp = SphericalUtil.computeOffset(position.coordinate,width,90);
        baseVert.add(new Location3D(tmp,altitude));
        tmp = SphericalUtil.computeOffset(tmp,depth,180);
        baseVert.add(new Location3D(tmp,altitude));
        tmp = SphericalUtil.computeOffset(tmp,width,-90);
        baseVert.add(new Location3D(tmp,altitude));
        tmp = SphericalUtil.computeOffset(tmp,depth,0);
        baseVert.add(new Location3D(tmp,altitude));
        return baseVert;
    }

    private void extrudePath(List<Location3D> pointList, float distance) {
            Iterator iterator = pointList.iterator();
            Location3D p1,p2;
            while (iterator.hasNext()) {
                p1=(Location3D) iterator.next();
                p2 = new Location3D(p1.coordinate,p1.relativeAltitude+distance);
                edgesList.add(new Edge(p1,p2));
            }
    }

    private List<Location3D> moveInZAxis (List<Location3D> pointList, float distance) {
        List<Location3D> movedPoints=new ArrayList<>();
        Iterator iterator = pointList.iterator();
        Location3D p1,p2;
        while (iterator.hasNext()) {
            p1=(Location3D) iterator.next();
            p2 = new Location3D(p1.coordinate,p1.relativeAltitude+distance);
            movedPoints.add(p2);
        }
        return movedPoints;
    }


}
