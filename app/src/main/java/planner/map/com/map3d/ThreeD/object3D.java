package planner.map.com.map3d.ThreeD;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public abstract class object3D extends Form {
    protected Location3D position;
    protected float[] rotation={0,0,0};

    public float pitagoras(float d1,float d2) {
        return (float) Math.sqrt(Math.pow(d1,2)+Math.pow(d2,2));
    }

}
