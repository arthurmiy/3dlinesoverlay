package planner.map.com.map3d.MapInfosGetter;

import android.graphics.Point;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class MapInfosGoogleMaps implements MapInfosInterface {
    private double tilt;
    private LatLng middleTop=new LatLng(0,0);
    private LatLng middleBottom=new LatLng(0,0);
    private LatLng center=new LatLng(0,0);


    public MapInfosGoogleMaps(GoogleMap mMap, int width, int height) {
        double tilt = (double) Math.toRadians(mMap.getCameraPosition().tilt);
        if (tilt!=0) {
            Point centerLin = new Point(width / 2, height / 2);//
            Point pt = new Point();
            pt.set(centerLin.x, centerLin.y - height / 2);
            LatLng middleTop = mMap.getProjection().fromScreenLocation(pt);
            pt.set(centerLin.x, centerLin.y + height / 2);
            LatLng middleBottom = mMap.getProjection().fromScreenLocation(pt);
            pt.set(centerLin.x - width/ 2, centerLin.y);
            LatLng center = mMap.getProjection().fromScreenLocation(centerLin);

        }
    }

    @Override
    public double getTilt() {
        return tilt;
    }

    @Override
    public LatLng getMiddleTop() {
        return middleTop;
    }

    @Override
    public LatLng getMiddleBottom() {
        return middleBottom;
    }

    @Override
    public LatLng getCenter() {
        return center;
    }
}
