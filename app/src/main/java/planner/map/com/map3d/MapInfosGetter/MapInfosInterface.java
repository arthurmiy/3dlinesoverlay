package planner.map.com.map3d.MapInfosGetter;

import com.google.android.gms.maps.model.LatLng;

public interface MapInfosInterface {
    public double getTilt();
    public LatLng getMiddleTop ();
    public LatLng getMiddleBottom ();
    public LatLng getCenter ();
}
