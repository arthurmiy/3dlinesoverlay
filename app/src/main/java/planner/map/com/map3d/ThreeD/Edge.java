package planner.map.com.map3d.ThreeD;

public class Edge {
    private Location3D vertice1;
    private Location3D vertice2;

    public Edge (Location3D v1, Location3D v2) {
        vertice1=v1;
        vertice2=v2;
    }

    public Location3D getVertice1() {
        return vertice1;
    }

    public Location3D getVertice2() {
        return vertice2;
    }

    public Location3D[] getVertices() {
        Location3D[] verticeArray = {vertice1,vertice2};
        return verticeArray;
    }
}
